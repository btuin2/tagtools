#!/usr/bin/env bash
# shellcheck disable=SC2207

# Completion script for tagtools with bash.

__tagtools_find_in_array() {
	local word=$1
	local arr=$2
	shift
	for e in "${arr[@]}"; do [[ "$e" == "$word" ]] && return 0; done
	return 1
}

_tagtools_completion()
{
	# normalize IFS
	local IFS=$' \t\n'

	local cur prev
	cur="${COMP_WORDS[$COMP_CWORD]}"
	prev="${COMP_WORDS[$COMP_CWORD-1]}"

	local edit_options_with_arg=("--title" "--artist" "--album"
								 "--comment" "--genre" "--year --track")
	local edit_options_without_arg=("--dry-run")
	local edit_options=("${edit_options_with_arg[@]}" "${edit_options_without_arg[@]}")

	local list_options_with_arg=()
	local list_options_without_arg=("--json" "--full-path")
	local list_options=("${list_options_with_arg[@]}" "${list_options_without_arg[@]}")

	if [[ "${#COMP_WORDS[@]}" -lt 2 ]]; then
		return
	fi

	if [[ "${#COMP_WORDS[@]}" -eq 2 ]]; then
		COMPREPLY=($(compgen -W "edit list" "${COMP_WORDS[1]}"))
	else
		if [[ ${COMP_WORDS[1]} == "edit" ]]; then
			if [[ "${cur:0:1}" == "-" ]]; then
				if ! __tagtools_find_in_array "${prev}" "${edit_options_with_arg[*]}"; then
					COMPREPLY=($(compgen -W "${edit_options[*]}" -- "${cur}"))
				else
					# If COMPREPLY is empty, it will use the
					# default completion set with complete -o
					COMPREPLY=()
				fi
			fi
		fi
		if [[ ${COMP_WORDS[1]} == "list" ]]; then
			if [[ "${cur:0:1}" == "-" ]]; then
				if ! __tagtools_find_in_array "${prev}" "${list_options_with_arg[*]}"; then
					COMPREPLY=($(compgen -W "${list_options[*]}" -- "${cur}"))
				else
					COMPREPLY=()
				fi
			fi
		fi
	fi

}
complete -o bashdefault -o default -F _tagtools_completion tagtools
