/* Copyright (C) 2023 BTuin
   This file is part of tagtools

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

#include <getopt.h>
#include <nlohmann/json.hpp>
#include <string_view>
#include <taglib/fileref.h>
#include <taglib/tag.h>
#include "config.h"

#include <cstdlib>
#include <exception>
#include <filesystem>
#include <iostream>
#include <list>
#include <optional>
#include <ostream>
#include <stdexcept>
#include <tuple>

using json = nlohmann::json;

static struct option long_options_edit[] = {
    {"title", required_argument, nullptr, 't'},
    {"artist", required_argument, nullptr, 'a'},
    {"album", required_argument, nullptr, 'A'},
    {"comment", required_argument, nullptr, 'c'},
    {"genre", required_argument, nullptr, 'g'},
    {"year", required_argument, nullptr, 'y'},
    {"track", required_argument, nullptr, 'T'},
    {"dry-run", no_argument, nullptr, 'd'},
    {nullptr, 0, nullptr, 0}};

static struct option long_options_list[] = {
    {"json", no_argument, nullptr, 'j'},
    {"full-path", no_argument, nullptr, 'f'},
    {nullptr, 0, nullptr, 0}};

struct edit_options {
	bool dry_run = false;
	std::optional<TagLib::String> title;
	std::optional<TagLib::String> artist;
	std::optional<TagLib::String> album;
	std::optional<TagLib::String> comment;
	std::optional<TagLib::String> genre;
	std::optional<unsigned int> year;
	std::optional<unsigned int> track;
};

struct list_options {
	bool json = false;
	bool full_path = false;
};

struct file_wrapper {
	std::string path;
	TagLib::FileRef file_ref;
	file_wrapper(std::string path, TagLib::FileRef &file)
	    : path{path}, file_ref{file} {}
};

inline unsigned int stoui(const std::string &s) {
	unsigned long lresult = stoul(s, 0, 10);
	unsigned int result = lresult;
	if (result != lresult)
		throw std::out_of_range("Number too big");
	return result;
}

TagLib::String option_to_tag_string(const char *str) {
	return str ? TagLib::String(str, TagLib::String::Type::UTF8) : "";
}

std::string to_string_safe(const char *str) {
	return str ? std::string(str) : "";
}

void set_tag(TagLib::FileRef &file, const edit_options &tag) {
	if (tag.title)
		file.tag()->setTitle(tag.title.value());
	if (tag.artist)
		file.tag()->setArtist(tag.artist.value());
	if (tag.album)
		file.tag()->setAlbum(tag.album.value());
	if (tag.comment)
		file.tag()->setComment(tag.comment.value());
	if (tag.genre)
		file.tag()->setGenre(tag.genre.value());
	if (tag.year)
		file.tag()->setYear(tag.year.value());
	if (tag.track)
		file.tag()->setTrack(tag.track.value());
}

constexpr auto modifier = [](std::ostream &s) -> std::ostream & {
	return s << std::setw(10);
};

void print_tag(std::ostream &os, const TagLib::Tag &tag) {


	os << modifier << "Title: " << tag.title().to8Bit(true) << "\n";
	os << modifier << "Artist: " << tag.artist().to8Bit(true) << "\n";
	os << modifier << "Album: " << tag.album().to8Bit(true) << "\n";
	os << modifier << "Comment: " << tag.comment().to8Bit(true) << "\n";
	os << modifier << "Genre: " << tag.genre().to8Bit(true) << "\n";
	os << modifier << "Year: " << tag.year() << "\n";
	os << modifier << "Track: " << tag.track() << "\n";
}

void print_tags(const std::list<file_wrapper> &files) {
	bool first = true;
	for (const auto &file : files) {
		if (!first) {
			std::cout << "\n";
		} else {
			first = false;
		}
		std::cout << modifier << "File: " << file.path << "\n";
		print_tag(std::cout, *file.file_ref.tag());
	}
}

void to_json(json &j, const file_wrapper &file) {
	const TagLib::Tag *t = file.file_ref.tag();
	j = json{{"file", file.path},
	         {"title", t->title().to8Bit(true)},
	         {"artist", t->artist().to8Bit(true)},
	         {"album", t->album().to8Bit(true)},
	         {"comment", t->comment().to8Bit(true)},
	         {"genre", t->genre().to8Bit(true)},
	         {"year", t->year()},
	         {"track", t->track()}};
}

void print_tags_json(const std::list<file_wrapper> &files) {
	auto arr = json::array();
	for (const file_wrapper &file : files) {
		arr.push_back(file);
	}

	std::cout << arr << "\n";
}

void print_help() {
	std::cerr << "Usage:\n"
	             "tagtools edit [OPTIONS]... FILES...\n"
	             "tagtools list [OPTIONS]... FILES...\n";
}

void print_version() { std::cerr << "Version " << VERSION << std::endl; }

/* Manage the flags "--help" and "--version".
Return true if there is such a flag.
We cannot use getopt to parse --help and --version, because
it permutes non-arguments to the end of argv, meaning that
"list" and "edit" would be put at the end.
 */
bool parse_standard_options(int argc, char **argv) {
	for (int i = 0; i < argc; i++) {
		std::string_view arg(argv[i]);
		if (arg == "--") {
			break;
		}
		if (arg == "-h" || arg == "--help") {
			print_help();
			return true;
		} else if (arg == "-v" || arg == "--version") {
			print_version();
			return true;
		}
	}
	return false;
}

edit_options parse_options_edit(int argc, char **argv) {
	optind = 0;
	opterr = 1;
	int c = 0;

	edit_options options;

	while ((c = getopt_long(argc, argv, "t:a:A:c:g:y:T:l", long_options_edit,
	                        nullptr)) != -1) {
		switch (c) {
		case 'd':
			options.dry_run = true;
			break;
		case 't':
			options.title = option_to_tag_string(optarg);
			break;
		case 'a':
			options.artist = option_to_tag_string(optarg);
			break;
		case 'A':
			options.album = option_to_tag_string(optarg);
			break;
		case 'c':
			options.comment = option_to_tag_string(optarg);
			break;
		case 'g':
			options.genre = option_to_tag_string(optarg);
			break;
		case 'y':
			try {
				options.year = stoui(to_string_safe(optarg));
			} catch (const std::exception &e) {
				throw std::invalid_argument("Invalid date: " +
				                            std::string(optarg));
			}
			break;
		case 'T':
			try {
				options.track = stoui(to_string_safe(optarg));
			} catch (const std::exception &e) {
				throw std::invalid_argument("Invalid track number: " +
				                            std::string(optarg));
			}
			break;
		case 'h':
			print_help();
			break;
		default:
			throw std::invalid_argument("Invalid argument");
		}
	}
	opterr = 1;
	return options;
}

list_options parse_options_list(int argc, char **argv) {
	optind = 0;
	opterr = 1;
	int c = 0;

	list_options options;

	while ((c = getopt_long(argc, argv, "fj", long_options_list, nullptr)) !=
	       -1) {
		switch (c) {
		case 'j':
			options.json = true;
			break;
		case 'f':
			options.full_path = true;
			break;
		default:
			throw std::invalid_argument("Invalid argument");
		}
	}
	return options;
}

void edit_tags(int argc, char **argv) {
	optind = 0;
	edit_options tag = parse_options_edit(argc, argv);

	if (optind == argc) {
		throw std::invalid_argument("No file name specified");
	}
	std::list<file_wrapper> files;

	for (int i = optind; i < argc; i++) {

		std::filesystem::path path = std::filesystem::current_path() / argv[i];

		if (!std::filesystem::exists(path)) {
			throw std::invalid_argument("File does not exist: " +
			                            std::string(argv[i]));
		}

		TagLib::FileRef file(argv[i]);
		files.push_back(file_wrapper(std::string(argv[i]), file));
		set_tag(file, tag);
	}

	if (tag.dry_run) {
		print_tags(files);
	} else {
		for (auto &file : files) {
			file.file_ref.save();
		}
	}
}

void list_tags(int argc, char **argv) {
	optind = 0;
	list_options options = parse_options_list(argc, argv);

	if (optind == argc) {
		throw std::invalid_argument("No file name specified");
	}

	std::list<file_wrapper> files;

	TagLib::FileRef f;

	for (int i = optind; i < argc; i++) {

		auto current_path = std::filesystem::current_path();

		std::filesystem::path path = current_path / argv[i];
		path = path.lexically_normal();

		if (!std::filesystem::exists(path)) {
			throw std::invalid_argument("File does not exist: " +
			                            std::string(argv[i]));
		}

		TagLib::FileRef file(argv[i]);
		if (file.isNull()) {
			throw std::invalid_argument(
			    "File cannot be accessed or cannot be tagged: " +
			    std::string(argv[i]));
		}

		if (!options.full_path) {
			path = path.lexically_relative(current_path);
		}

		files.push_back(file_wrapper(path, file));
	}

	if (options.json) {
		print_tags_json(files);
	} else {
		print_tags(files);
	}
}

int main(int argc, char **argv) {
	std::string action;
	if (argc >= 2) {
		action = std::string(argv[1]);
	}

	if (parse_standard_options(argc, argv)) {
		return EXIT_SUCCESS;
	}

	if (argc < 2) {
		std::cerr << "Incorrect argument number." << std::endl;
		print_help();
		return EXIT_FAILURE;
	}

	try {
		if (action == "edit") {
			edit_tags(argc - 1, argv + 1);
		} else if (action == "list") {
			list_tags(argc - 1, argv + 1);
		} else {
			std::cerr << "Incorrect usage:" << std::endl;
			print_help();
			return EXIT_FAILURE;
		}
	} catch (const std::exception &e) {
		std::cerr << "Error: " << e.what() << std::endl;
	}
}
